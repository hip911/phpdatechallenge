<?php

namespace hip911\DateChallenge;


class DateDiffCalculator
{
    /**
     * @var Date
     */
    private $startDate;
    /**
     * @var Date
     */
    private $endDate;

    /**
     * DateDiffCalculator constructor.
     * @param Date $startDate
     * @param Date $endDate
     */
    public function __construct(Date $startDate, Date $endDate)
    {
        $this->startDate    = $startDate;
        $this->endDate      = $endDate;
    }

    /**
     * @return int
     */
    public function diffYears()
    {
        return $this->endDate->getYear() - $this->startDate->getYear() - $this->currentYearCorrection();
    }

    /**
     * @return int
     */
    public function diffMonths()
    {
        if ($this->currentYearCorrection() === 1) {
            return 12 - $this->startDate->getMonth() + $this->endDate->getMonth() - $this->currentMonthCorrection();
        }

        return $this->endDate->getMonth() - $this->startDate->getMonth();
    }

    /**
     * @return int
     */
    public function diffDays()
    {
        if ($this->currentMonthCorrection() === 1) {
            return Date::$monthLengths[$this->startDate->getMonthCode()] - $this->startDate->getDay() + $this->endDate->getDay();
        }

        return $this->endDate->getDay() - $this->startDate->getDay();
    }

    /**
     * @return int
     */
    public function diffTotalDays()
    {
        return $this->calcDaysSinceEpoch($this->endDate) - $this->calcDaysSinceEpoch($this->startDate);
    }

    /**
     * @return bool
     */
    public function isInvert(){
        if($this->startDate->getYear() > $this->endDate->getYear()) {
            return true;
        }
        if($this->startDate->getYear() === $this->endDate->getYear() &&
           $this->startDate->getMonth() > $this->endDate->getMonth()) {
            return true;
        }
        if($this->startDate->getYear() === $this->endDate->getYear() &&
            $this->startDate->getMonth() === $this->endDate->getMonth() &&
            $this->startDate->getDay() > $this->endDate->getDay()) {
            return true;
        }

        return false;
    }

    /**
     * @return int
     */
    private function currentYearCorrection(){
        if ($this->startDate->getMonth() < $this->endDate->getMonth()) {
            return 0;
        }
        if ($this->startDate->getMonth() > $this->endDate->getMonth()) {
            return 1;
        }

        return $this->currentMonthCorrection();
    }

    /**
     * @return int
     */
    private function currentMonthCorrection()
    {
        if ($this->startDate->getDay() > $this->endDate->getDay()) {
            return 1;
        }

        return 0;
    }

    /**
     * @param Date $date
     * @return int
     */
    private function calcDaysSinceEpoch(Date $date)
    {
        $y = $date->getYear();
        $m = $date->getMonth();
        $d = $date->getDay();

        if($m < 3){
            $m = $m + 12;
            $y = $y - 1;
        }
        return (int) (365 * $y + floor($y/4) - floor($y/100) + floor($y/400) + $d + floor((153*$m+8)/5));
    }
}
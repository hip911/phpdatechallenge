<?php

  spl_autoload_register(
    function($class) {
      static $classes = null;
      if ($classes === null) {
        $classes = array(
          'mydate' => '/MyDate.php',
          'hip911\datechallenge\dateparser' => '/DateParser.php',
          'hip911\datechallenge\date' => '/Date.php',
          'hip911\datechallenge\datediffcalculator' => '/DateDiffCalculator.php',
          'hip911\datechallenge\exception\dateparseexception' => '/DateParseException.php'
        );
      }
      $cn = strtolower($class);
      if (isset($classes[$cn])) {
        require __DIR__ . $classes[$cn];
      }
    }  
  );

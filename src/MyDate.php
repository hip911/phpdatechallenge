<?php

use hip911\DateChallenge\DateDiffCalculator;
use hip911\DateChallenge\DateParser;

class MyDate {

    public static function diff($start, $end) {

        $startDate = DateParser::createFromString($start);
        $endDate = DateParser::createFromString($end);

        $dateCalculator = new DateDiffCalculator($startDate,$endDate);

      // Sample object:
      return (object)array(
        'years' => $dateCalculator->diffYears(),
        'months' => $dateCalculator->diffMonths(),
        'days' => $dateCalculator->diffDays(),
        'total_days' => $dateCalculator->diffTotalDays(),
        'invert' => $dateCalculator->isInvert()
      );

    }

  }

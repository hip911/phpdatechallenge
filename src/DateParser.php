<?php

namespace hip911\DateChallenge;

use hip911\DateChallenge\Exception\DateParseException;

class DateParser
{
    /**
     * @param $dateString
     * @return \hip911\DateChallenge\Date
     * @throws \hip911\DateChallenge\Exception\DateParseException
     */
    public static function createFromString($dateString)
    {
        $parts = explode('/',$dateString);

        if (count($parts) !== 3) {
            throw new DateParseException('Cannot parse date');
        }

        return new Date($parts[0],$parts[1],$parts[2]);
    }
}
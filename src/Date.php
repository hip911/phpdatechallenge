<?php

namespace hip911\DateChallenge;

class Date
{
    public static $monthLengths = [
        '1' => 31,
        '2' => 28,
        '2l'=> 29,
        '3' => 31,
        '4' => 30,
        '5' => 31,
        '6' => 30,
        '7' => 31,
        '8' => 31,
        '9' => 30,
        '10' => 31,
        '11' => 30,
        '12' => 31
    ];
    /**
     * @var int $year
     */
    private $year;
    /**
     * @var int $month
     */
    private $month;
    /**
     * @var int $day
     */
    private $day;

    /**
     * Date constructor.
     * @param $year
     * @param $month
     * @param $day
     */
    public function __construct($year, $month, $day)
    {
        $this->setYear((int)$year);
        $this->setMonth((int)$month);
        $this->setDay((int)$day);
    }

    /**
     * @return int
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @return string
     */
    public function getMonthCode()
    {
        return $this->getMonth() . $this->getLeapYearCode();
    }

    /**
     * @return int
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    private function setYear($year)
    {
        if ($year < 1) {
            throw new \InvalidArgumentException("B.C. years are not supported");
        }
        $this->year = $year;
    }

    /**
     * @param int $month
     */
    private function setMonth($month)
    {
        if (!in_array($month, range(1, 12, 1))) {
            throw new \InvalidArgumentException("Month must be between 1-12");
        }
        $this->month = $month;
    }

    /**
     * @param int $day
     */
    private function setDay($day)
    {
        if(self::$monthLengths[$this->getMonthCode()] < $day){
            throw new \InvalidArgumentException("There is no such day in this month");
        }

        $this->day = $day;
    }

    /**
     * @return string
     */
    private function getLeapYearCode()
    {
        return $this->getMonth() === 2 && $this->isLeapYear() ? 'l' : '';
    }

    /**
     * @return bool
     */
    private function isLeapYear()
    {
        if ($this->getYear() % 400 === 0) {
            return true;
        }
        if ($this->getYear() % 100 === 0) {
            return false;
        }
        if ($this->getYear() % 4 === 0) {
            return true;
        }

        return false;
    }

}
<?php

use hip911\DateChallenge\Date;
use hip911\DateChallenge\DateDiffCalculator;

class DateDiffCalculatorTest extends PHPUnit_Framework_TestCase
{
    private $startDate;
    private $endDate;
    private $dateDiffCalculator;

    public function setUp()
    {
        $this->startDate  = Mockery::mock(Date::class);
        $this->endDate    = Mockery::mock(Date::class);
        $this->dateDiffCalculator = new DateDiffCalculator($this->startDate, $this->endDate);

        parent::setUp();
    }

    public function testDiffYearsSimple()
    {
        $this->mockDate($this->startDate,1,1,1);
        $this->mockDate($this->endDate,5,5,1);

        $this->assertEquals(4,$this->dateDiffCalculator->diffYears());
    }

    public function testDiffYearsComplex()
    {
        $this->mockDate($this->startDate,1,5,1);
        $this->mockDate($this->endDate,5,1,1);

        $this->assertEquals(3,$this->dateDiffCalculator->diffYears());
    }

    public function testDiffYearsSameMonthSimple()
    {
        $this->mockDate($this->startDate,1,5,5);
        $this->mockDate($this->endDate,5,5,5);

        $this->assertEquals(4,$this->dateDiffCalculator->diffYears());
    }

    public function testDiffYearsSameMonthComplex()
    {
        $this->mockDate($this->startDate,1,5,6);
        $this->mockDate($this->endDate,5,5,5);

        $this->assertEquals(3,$this->dateDiffCalculator->diffYears());
    }

    public function testDiffMonthsSimple()
    {
        $this->mockDate($this->startDate,1,1,1);
        $this->mockDate($this->endDate,1,5,5);

        $this->assertEquals(4,$this->dateDiffCalculator->diffMonths());
    }

    public function testDiffMonthsComplex()
    {
        $this->mockDate($this->startDate,1,5,6);
        $this->mockDate($this->endDate,5,1,5);

        $this->assertEquals(7,$this->dateDiffCalculator->diffMonths());
    }

    public function testDiffDaysSimple()
    {
        $this->mockDate($this->startDate,1,1,1);
        $this->mockDate($this->endDate,1,5,5);

        $this->assertEquals(4,$this->dateDiffCalculator->diffDays());
    }

    public function testDiffDaysComplex()
    {
        $this->mockDate($this->startDate,1,5,6);
        $this->startDate->shouldReceive('getMonthCode')->once()->andReturn('5');
        $this->mockDate($this->endDate,5,5,5);

        $this->assertEquals(30,$this->dateDiffCalculator->diffDays());
    }

    public function testIsInvertYears()
    {
        $this->mockDate($this->startDate,2,1,1);
        $this->mockDate($this->endDate,1,1,1);

        $this->assertEquals(true,$this->dateDiffCalculator->isInvert());
    }

    public function testIsInvertMonths()
    {
        $this->mockDate($this->startDate,1,2,1);
        $this->mockDate($this->endDate,1,1,1);

        $this->assertEquals(true,$this->dateDiffCalculator->isInvert());
    }

    public function testIsInvertDays()
    {
        $this->mockDate($this->startDate,1,1,2);
        $this->mockDate($this->endDate,1,1,1);

        $this->assertEquals(true,$this->dateDiffCalculator->isInvert());
    }

    public function testNotInvert()
    {
        $this->mockDate($this->startDate,1,1,1);
        $this->mockDate($this->endDate,1,1,2);

        $this->assertEquals(false,$this->dateDiffCalculator->isInvert());
    }
    //2013/01/01

    public function testTotalDays()
    {
        $this->mockDate($this->startDate,2013,01,01);
        $this->mockDate($this->endDate,2029,05,15);

        $this->assertEquals(5978,$this->dateDiffCalculator->diffTotalDays());
    }


    private function mockDate($date,$year,$month,$day)
    {
        $date->shouldReceive('getYear')->andReturn($year);
        $date->shouldReceive('getMonth')->andReturn($month);
        $date->shouldReceive('getDay')->andReturn($day);
    }

    public function tearDown()
    {
        Mockery::close();
        parent::tearDown();
    }

}

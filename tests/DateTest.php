<?php

use hip911\DateChallenge\Date;

class DateTest extends PHPUnit_Framework_TestCase
{
    public function testYearMustBeAnIntegerGreaterThanOne()
    {
        $this->setExpectedException(InvalidArgumentException::class);
        $date = new Date('-2','03','01');
    }

    public function testMonthMustBeValid()
    {
        $this->setExpectedException(InvalidArgumentException::class);
        $date = new Date('2000','13','01');
    }

    public function testDayMustBeValid()
    {
        $this->setExpectedException(InvalidArgumentException::class);
        $date = new Date('2000','09','43');
    }

    public function testNotLeapYear()
    {
        $this->setExpectedException(InvalidArgumentException::class);
        $date = new Date('1999','02','29');
    }

    public function testAllGettersWorkCorrectly()
    {
        $year = '1995';
        $month = '05';
        $day = '21';
        $date = new Date($year,$month,$day);
        $this->assertEquals($year,$date->getYear());
        $this->assertEquals($month,$date->getMonth());
        $this->assertEquals($day,$date->getDay());
    }

    public function testYearDivisibleBy400IsALeapYear()
    {
        $date = new Date('2000','02','29');
        $this->assertInstanceOf(Date::class,$date);
    }

    public function testYearDivisibleBy100ButNot400IsNotALeapYear()
    {
        $this->setExpectedException(InvalidArgumentException::class);
        $date = new Date('1900','02','29');
    }

    public function testYearDivisibleBy4ButNot100IsALeapYear()
    {
        $date = new Date('2004','02','29');
        $this->assertInstanceOf(Date::class,$date);
    }


//    private static function makeDateFrom($year,$month,$day){
//        return new Date($year,$month,$day);
//    }
}

<?php

use hip911\DateChallenge\Date;
use hip911\DateChallenge\Exception\DateParseException;
use hip911\DateChallenge\DateParser;


class DateParserTest extends PHPUnit_Framework_TestCase
{
    public function testParsingDatesInWrongFormatFails()
    {
        $this->setExpectedException(DateParseException::class);
        DateParser::createFromString('2000-12-01');
    }

    /* integration */
    public function testParsingDatesFromStringReturnsValidObject()
    {
        $date = DateParser::createFromString('2000/12/01');
        $this->assertInstanceOf(Date::class,$date);
    }

}
